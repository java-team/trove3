Source: trove3
Section: java
Priority: optional
Maintainer: Debian Java Maintainers <pkg-java-maintainers@lists.alioth.debian.org>
Uploaders:
 Erich Schubert <erich@debian.org>,
 Torsten Werner <twerner@debian.org>
Build-Depends:
 ant,
 ant-optional,
 debhelper (>= 11),
 default-jdk,
 javahelper,
 junit4,
 maven-repo-helper
Standards-Version: 4.2.1
Vcs-Git: https://anonscm.debian.org/git/pkg-java/trove3.git
Vcs-Browser: https://anonscm.debian.org/cgit/pkg-java/trove3.git
Homepage: http://trove4j.sourceforge.net/

Package: libtrove3-java
Architecture: all
Depends: ${misc:Depends}
Description: high performance collections for java
 GNU Trove is a fast, lightweight  implementations of the java.util
 Collections API. These implementations are designed to be pluggable
 replacements for their JDK equivalents.
 .
 Whenever possible, GNU Trove provide the same collections support for
 primitive types. This gap in the JDK is often addressed by using the
 "wrapper" classes (java.lang.Integer, java.lang.Float, etc.) with
 Object-based collections. For most applications, however, collections
 which store primitives directly will require less space and yield
 significant performance gains.

Package: libtrove3-java-doc
Section: doc
Architecture: all
Depends: ${misc:Depends}
Suggests: libtrove3-java
Description: high performance collections for java
 GNU Trove is a fast, lightweight  implementations of the java.util
 Collections API. These implementations are designed to be pluggable
 replacements for their JDK equivalents.
 .
 Whenever possible, GNU Trove provide the same collections support for
 primitive types. This gap in the JDK is often addressed by using the
 "wrapper" classes (java.lang.Integer, java.lang.Float, etc.) with
 Object-based collections. For most applications, however, collections
 which store primitives directly will require less space and yield
 significant performance gains.
 .
 This package includes the documentation.
